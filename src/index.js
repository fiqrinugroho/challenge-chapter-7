import React from 'react';
import ReactDOM from 'react-dom/client';
import 'bootstrap/dist/css/bootstrap.min.css'
import {
  BrowserRouter,
  Routes,
  Route
} from 'react-router-dom';
import { Provider } from "react-redux";

import './index.css';
import App from './App';
import RentCar from './components/RentCar';
import store from "./store/index";


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <Provider store={store}>
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<App />}></Route>
        <Route path='/cars' element={<RentCar />}></Route>
      </Routes>
    </BrowserRouter>
  </Provider>
);

