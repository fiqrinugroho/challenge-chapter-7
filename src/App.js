import './App.css'
import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";

import React from 'react';

import Main from './components/';

function App() {

  return (
    <Main />
  );
}

export default App;
