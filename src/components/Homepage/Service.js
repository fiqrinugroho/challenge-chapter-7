import React from 'react';
import classes from './css/Service.module.css'; 

const Service = (props) => {
  return (
    <div className="container" id="our_services">
      <div className="row">
        <div className={`${classes.girlImg} col-md-6`}>
          <img src="/../../../images/girl.png" className={classes.girl} alt="girl"/>
        </div>
        <div className={`col-md-6 ${classes.ourServices}`}>
          <h3>Best Car Rental for any kind of trip in Cikampek!</h3>
          <p>Sewa mobil di cikampek bersama Binar Car Rental jaminan harga lebih murah 
            dibandingkan yang lain, kondisi mobil baru, 
           serta kualitas pelayanan terbaik untuk perjalanan wisata, bisnis, wedding, meeting, dll.
          </p>
          <ul className={`list-group list-group-flush ${classes.listServices}`}>
            <li className="list-item"><img src="/../../../icon/icon_check_list.svg" alt='checkList' className={`classes.listImg`}/>Mobil Dengan Supir di Bali 12 Jam</li>
            <li className="list-item"><img src="/../../../icon/icon_check_list.svg" alt='checkList' className={`classes.listImg`}/>Sewa Mobil Lepas Kunci di Bali 24 Jam</li>
            <li className="list-item"><img src="/../../../icon/icon_check_list.svg" alt='checkList' className={`classes.listImg`}/>Sewa Mobil Jangka Panjang Bulanan</li>
            <li className="list-item"><img src="/../../../icon/icon_check_list.svg" alt='checkList' className={`classes.listImg`}/>Gratis Antar - Jemput Mobil di Bandara</li>
            <li className="list-item"><img src="/../../../icon/icon_check_list.svg" alt='checkList' className={`classes.listImg`}/>Layanan Airport Transfer / Drop In Out</li>
          </ul>
        </div>
      </div>
    </div>
  )
}

export default Service