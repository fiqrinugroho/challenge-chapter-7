import React from "react";
import classes from "./css/Testimoni.module.css";
import OwlCarousel from "react-owl-carousel";

const Testimoni = () => {
  return (
    <div className={classes.mainTestimoni} id="testimonial">
      <div className="container mt-5">
        <div className="row">
          <div className="col-12">
            <h4>Testimonial</h4>
            <p>Berbagai review positif dari para pelanggan kami</p>
          </div>
        </div>
      </div>
      <section className="container-fluid">
        <div className={classes.mainTestimoniCarousel}>
          <OwlCarousel
            className="owl-theme"
            loop
            nav
            center
            dots={false}
            margin={32}
            navText={[`<img src="/../../../icon/icon_left.svg" width='32px' height='32px'>`, `<img src="/../../../icon/icon_right.svg" width='32px' height='32px'>`]}
            responsive={{
              0: {
                items: 1,
              },
              600: {
                items: 2,
              },
              1000: {
                items: 2,
              },
            }}
          >
            <div className={classes.item}>
              <div className={classes.mainTestimoniDeks}>
                <div className="row">
                  <div className="col-2 offset-3">
                    <img className="img-fluid" src="/../../../icon/icon_star.svg" alt="icon rate" />
                  </div>
                </div>
                <div className="row">
                  <div className="col-2">
                    <div className={classes.mainTestimoniImages}>
                      <img src="/../../../images/jhon_doe.png" alt="jhon doe" />
                    </div>
                  </div>
                  <div className="col-9 offset-1">
                    <p>
                      “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”
                    </p>
                  </div>
                </div>
                <div className="row">
                  <div className={classes.mainTestimoniName}>
                    <div className="col- offset-3">
                      <p>John Dee 32, Bromo</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className={classes.item}>
              <div className={classes.mainTestimoniDeks}>
                <div className="row">
                  <div className="col-2 offset-3">
                    <img className="img-fluid" src="/../../../icon/icon_star.svg" alt="icon rate" />
                  </div>
                </div>
                <div className="row">
                  <div className="col-2">
                    <div className={classes.mainTestimoniImages}>
                      <img src="/../../../images/jane.png"  alt="uwong Grils" />
                    </div>
                  </div>
                  <div className="col-9 offset-1">
                    <p>
                      “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”
                    </p>
                  </div>
                </div>
                <div className="row">
                  <div className={classes.mainTestimoniName}>
                    <div className="col- offset-3">
                      <p>John Dee 32, Bromo</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className={classes.item}>
              <div className={classes.mainTestimoniDeks}>
                <div className="row">
                  <div className="col-2 offset-3">
                    <img className="img-fluid" src="/../../../icon/icon_star.svg" alt="icon rate" />
                  </div>
                </div>
                <div className="row">
                  <div className="col-2">
                    <div className={classes.mainTestimoniImages}>
                      <img src="/../../../images/saskeh.png" alt="Saskeh" />
                    </div>
                  </div>
                  <div className="col-9 offset-1">
                    <p>
                      “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”
                    </p>
                  </div>
                </div>
                <div className="row">
                  <div className={classes.mainTestimoniName}>
                    <div className="col- offset-3">
                      <p>Uciha Sasuke 12, Konoha</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </OwlCarousel>
        </div>
      </section>
    </div>
  );
};
export default Testimoni;
