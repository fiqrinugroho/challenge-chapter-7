import React from 'react';
import classes from './css/WhyUs.module.css'; 

const WhyUs = (props) => {
  return (
    <div className="container" id="why_us">
      <div className={classes.whyus}>
        <h3>Why Us?</h3>
        <p>Mengapa harus pilih Binar Car Rental?</p>
      </div>
        <div className="row">
          <div className="col-md-3">
            <div className="card rounded-3 ">
              <div className={classes.cardBody}>
                <img src="/../../../icon/icon_complete.svg" className="mb-3" alt=""/>
                <h1 className={classes.cardTitle}>Mobil Lengkap</h1>
                <p>Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat</p>
              </div>
            </div>
          </div>
          <div className="col-md-3">
            <div className="card rounded-3">
              <div className={classes.cardBody}>
                <img src="/../../../icon/icon_price.svg" className="mb-3" alt=""/>
                <h1 className={classes.cardTitle}>Harga Murah</h1>
                <p>Harga murah dan bersaing, bisa bandingkan harga kami dengan rental mobil lain</p>
              </div>
            </div>  
          </div>  
          <div className="col-md-3">     
            <div className="card rounded-3">
              <div className={classes.cardBody}>
                <img src="/../../../icon/icon_24hrs.svg" className="mb-3" alt=""/>
                <h1 className={classes.cardTitle}>Layanan 24 jam</h1>
                <p>Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga tersedia di akhir minggu</p>
              </div>
            </div>
          </div>
          <div className="col-md-3">
            <div className="card rounded-3 ">
              <div className={classes.cardBody}>
                <img src="/../../../icon/icon_professional.svg" className="mb-3" alt=""/>
                <h1 className={classes.cardTitle}>Sopir Profesional</h1>
                <p>Sopir yang profesional, berpengalaman, jujur, ramah dan selalu tepat waktu</p>
              </div>
            </div>
          </div>
        </div>
    </div>
  )
}

export default WhyUs
