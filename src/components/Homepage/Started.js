import React from 'react';
import Button from '../UI/Button';
import classes from './css/Started.module.css'; 

const Started = (props) => {
  return (
    <div className={`container rounded-3 ${classes.jumbotron}`}>
      <div className={classes.textJumbotron}>
        <h1 className="">Sewa Mobil di Cikampek Sekarang</h1>
        <p className="">Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
          sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
        </p>
        <a href="/cars">
				<Button className={classes.buttonStarted}>Mulai Sewa Mobil</Button>
        </a>
      </div>
    </div>
  )
}

export default Started