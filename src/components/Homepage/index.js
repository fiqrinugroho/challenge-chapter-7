import Faq from "./Faq";
import Service from "./Service";
import Started from "./Started";
import Testimoni from "./Testimoni";
import WhyUs from "./WhyUs";


const HomePage = ({ onKlik }) => {

  return (
    <section className="HomePage">
      <Service />
      <WhyUs />
      <Testimoni />
      <Started />
      <Faq />
    </section>
  );
};
export default HomePage;
