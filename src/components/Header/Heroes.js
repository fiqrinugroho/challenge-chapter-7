import React from 'react';
import Button from '../UI/Button';
import classes from './css/Heroes.module.css'; 
import Navigations from './Navigations'

const Heroes = (props) => {
	return (
    <header>
      <Navigations />
      <section className={classes.hero}>
        <div className="container">
          <div className="row">
            <div className="col-md-6 hero-tagline">
              <div className={classes.textHero}>
                <h1 className={classes.title}>Sewa & Rental Mobil Terbaik di Kawasan Cikampek</h1>
                <p className={classes.welcome}>Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.</p>
                <a href="/cars">
                 <Button className={classes.buttonHero} >Mulai Sewa Mobil</Button>
                 </a>
              </div>
            </div>
            <div className={`col-md-6 ${classes.car}`}>
              <img src="/../../../images/car.png" alt="car" width="117%"/>
            </div>
          </div>
        </div>
      </section>
    </header>
	)
}
export default Heroes