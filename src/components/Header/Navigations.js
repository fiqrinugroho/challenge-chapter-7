import React from 'react';
import Buttons from '../UI/Button';
import classes from './css/Navigations.module.css'; 
import {
  Navbar,
  NavbarBrand,
  NavItem,
  Nav,
  NavLink,
} from 'reactstrap';


const Navigations = (props) => {
  return (
    <>
      <Navbar className={`${classes.navbar} navbar-expand-lg position-fixed w-100 light`}>
        <Nav className="container">
          <NavbarBrand className="navbar-brand" href="/">
            <img src="/../../../images/logo.png" alt="logo" height="34"/>
          </NavbarBrand>
          <NavItem className="navbar-nav ms-auto">
            <NavLink className="">
              <a className="nav-link active" href="#our_services">Our Services</a>
            </NavLink>
            <NavLink className={classes.navText}>
              <a className="nav-link active" href="#why_us">Why Us</a>
            </NavLink>
            <NavLink className={classes.navText}>
              <a className="nav-link active" href="#testimonial">Testimonial</a>
            </NavLink>
            <NavLink className={classes.navText}>
              <a className="nav-link active" href="#faq">FAQ</a>
            </NavLink>
            <NavLink className={classes.navText}>
              <Buttons className="btn-normal">Register</Buttons>
            </NavLink>
          </NavItem>   
        </Nav>
      </Navbar>
      
    </>
  );
};

export default Navigations;