import React, { useEffect } from "react";
import Button from "../UI/Button";
import classes from "./Card.module.css";
import { useDispatch, useSelector } from "react-redux";
import { getPost } from "../../store/postSlice";

const Card = ({input}) => {
  const dispatch = useDispatch();
  const { posts, loading } = useSelector((state) => state.post);

  useEffect(() => {
    dispatch(getPost());
  }, [dispatch]);

  const filtered = posts.filter((car) => {

    return (
      new Date(Date.parse(car.availableAt.split("T")[0])) <
        new Date(input.availableDate) &&
      `${new Date(Date.parse(car.availableAt)).getHours()}:${new Date(
        Date.parse(car.availableAt)
      ).getMinutes()}` === input.timeRent &&
      (car.capacity >= input.capasity) &&
      (input.driver === '1'
        ? car.available
        : car.available === false)
    );
  });

  if (loading) {
    return <div className="loader"></div>;
  }

  if (input === "" || input === null) {
    return (
      <div className={classes.card}>
        <div className={`container mt-5 mb-3  ${classes.box}`}>
          <div className="row">
            {posts.map((cars) => (
              <div className="col-md-4 mt-3" key={cars.id}>
                <div className="card h-100 p-3 mb-2">
                  <img
                    src={`${cars.image}`}
                    className="card-img-top imgcar"
                    alt={cars.manufacture}
                  />
                  <div className="card-body">
                    <p className={classes.cardTitle}>{cars.manufacture + " " + cars.model} </p>
                    <h5 className="card-text ">Rp{" "}
                      {cars.rentPerDay
                        .toString()
                        .replace(/\B(?=(\d{3})+(?!\d))/g, ".")}{" "}
                      / Hari</h5>
                    <p className={classes.cardText}>{cars.description}</p>
                    <div className="col-lg-6 col-sm-12 ">
                      <div className="marklist-container col-sm-12">
                        <div className="d-flex">
                          <img
                            src="/../../../icon/fi_users.svg"
                            style={{ width: "20px" }}
                            alt="icon-pasengger"
                          />
                          <p className={`${classes.cardText} ${classes.iconText}`}>
                            {cars.capacity} orang
                          </p>
                        </div>
                        <div className="d-flex">
                          <img 
                            src="/../../../icon/fi_settings.svg"
                            style={{ width: "20px" }}
                            alt="icon-setting"
                          />
                          <p className={`${classes.cardText} ${classes.iconText}`}>
                            {cars.transmission}
                          </p>
                        </div>
                        <div className=" d-flex">
                          <img
                            src="/../../../icon/fi_calendar.svg"
                            style={{ width: "20px" }}
                            alt="icon-calendar"
                          />
                          <p className={`${classes.cardText} ${classes.iconText}`}>
                            Tahun {cars.year}
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="d-grid gap-2">
                    <Button className="btn btn-success">Pilih Mobil</Button>
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    )
  }else {
    return (
      <div className={classes.card}>
        <div className={`container mt-5 mb-3 box-sizing ${classes.box}`}>
          <div className="row">
            {filtered.map((cars) => (
              <div className="col-md-4" key={cars.id}>
                <div className="card p-3 mb-2">
                  <img
                    src={`${cars.image}`}
                    className="card-img-top imgcar"
                    alt={cars.manufacture}
                  />
                  <div className="card-body">
                    <p className={classes.cardTitle}>{cars.manufacture + " " + cars.model} </p>
                    <h5 className="card-text">Rp{" "}
                      {cars.rentPerDay
                        .toString()
                        .replace(/\B(?=(\d{3})+(?!\d))/g, ".")}{" "}
                      / Hari</h5>
                    <p className={classes.cardText}>{cars.description}</p>
                    <div className="col-lg-6 col-sm-12 ">
                      <div className="marklist-container col-sm-12">
                        <div className="d-flex">
                          <img
                            src="/../../../icon/fi_users.svg"
                            style={{ width: "20px" }}
                            alt="icon-pasengger"
                          />
                          <p className={`${classes.cardText} ${classes.iconText}`}>
                            {cars.capacity} orang
                          </p>
                        </div>
                        <div className="d-flex">
                          <img 
                            src="/../../../icon/fi_settings.svg"
                            style={{ width: "20px" }}
                            alt="icon-setting"
                          />
                          <p className={`${classes.cardText} ${classes.iconText}`}>
                            {cars.transmission}
                          </p>
                        </div>
                        <div className=" d-flex">
                          <img
                            src="/../../../icon/fi_calendar.svg"
                            style={{ width: "20px" }}
                            alt="icon-calendar"
                          />
                          <p className={`${classes.cardText} ${classes.iconText}`}>
                            Tahun {cars.year}
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="d-grid gap-2">
                    <Button className="btn btn-success">Pilih Mobil</Button>
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  }

};

export default Card;
