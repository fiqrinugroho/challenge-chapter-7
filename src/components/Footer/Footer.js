import React from 'react';
import classes from './Footer.module.css'; 

const Footer = (props) => {
	return (
    <footer>
      <div className={`container ${classes.foot}`} id="footer">
        <div className="row">
          <div className={`${classes.addres} col-md-3`}>
            <p className={`${classes.textFooter} ${classes.jalan}`}>
              Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000
            </p>
            <p className={classes.textFooter}>binarcarrental@gmail.com</p>
            <p className={classes.textFooter}>081-233-334-808</p>
          </div>
          <div className={`${classes.navFoot} col-md-2 row `}>
            <a className="nav-link text-nav-foot text-black" href="#our_services">Our services</a>
            <a className="nav-link text-nav-foot text-black" href="#why_us">Why Us</a>
            <a className="nav-link text-nav-foot text-black" href="#testimonial">Testimonial</a>
            <a className="nav-link text-nav-foot text-black" href="#faq">FAQ</a>
          </div>
          <div className={`col-md-4 ${classes.social}`}>
            <p className={`${classes.connect} text-footer`}>Connect with us</p>
            <a href="/" className={classes.iconSocial}><img src="/../../../icon/icon_facebook.svg" alt="icon_facebook"/></a>
            <a href="/" className={classes.iconSocial}><img src="/../../../icon/icon_instagram.svg" alt="icon_instagram"/></a>
            <a href="/" className={classes.iconSocial} ><img src="/../../../icon/icon_twitter.svg" alt="icon_twitter"/></a>
            <a href="/" className={classes.iconSocial}><img src="/../../../icon/icon_mail.svg" alt="icon_mail"/></a>
            <a href="/" className={classes.iconSocial}><img src="/../../../icon/icon_twitch.svg" alt="icon_twitch"/></a>
          </div>
          <div className={`col-md-3 ${classes.copyright}`}>
            <p className="text-footer copy-text">Copyright Binar 2022</p>
            <img src="/../../../images/logo.png" alt="logo" height="34"/>
          </div>
        </div>
      </div>
    </footer>
  )
}

export default Footer