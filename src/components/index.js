import React from "react";
import Footer from "./Footer/Footer";
import Heroes from "./Header/Heroes";
import Navigation from "./Header/Navigations";
import HomePage from "./HomePage";


const Main = (props) => {
  return (
    <>
    <Navigation />
    <Heroes />
    <HomePage />
    <Footer />
    </>
  )
};
export default Main;
