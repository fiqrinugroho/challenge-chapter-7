import React, {useState} from "react";
import Footer from "./Footer/Footer";
import Heroes from "./Header/Heroes";
import Navigation from "./Header/Navigations";
import Search from "./Content/SearchCars";
import Card from "./Content/Card";


const RentCar = (props) => {
  
const [input, setinput] = useState('')
const filter = (props) => {
  return setinput(props)
}
  return (
    <>
    <Navigation />
    <Heroes />
    <Search filter={filter} />
    <Card input={input} />
    <Footer />
    </>
  )
};
export default RentCar;
