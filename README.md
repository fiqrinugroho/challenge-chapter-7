# **BINAR CHALLENGE CHAPTER 7**
- Creating UI using React JS
- Display data from API
- Filter the data obtained without switching pages

# How To Run
#### 1. Clone the repo (use SSH)
   ```sh
   git clone git@gitlab.com:fiqrinugroho/challenge-chapter-7.git
   ```
#### 2. Install packages
   ```sh
   #for NPM user
   npm install
   ```

### Built With
* <a href="https://reactjs.org/">
    <img src ="https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB" alt="React JS"/>
  </a>
* <a href="redux-toolkit.js.org">
   <img src='https://raw.githubusercontent.com/reduxjs/redux/master/logo/logo-title-dark.png' alt='Redux Logo with Dark Title' width='150' height='50' />
  </a>

